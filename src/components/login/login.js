import React, { useState } from 'react';
import M from "materialize-css";
import './login.scss'
import { useHistory } from "react-router-dom";

export default function Login() {


    const history = useHistory();


    const [datos, setDatos] = useState({
        user:'',
        password:''
    })

    const handleInputChange = (event) =>{
        setDatos({
            ...datos,
            [event.target.name] : event.target.value
        })
    }

    const loginUser =(event) =>{
        event.preventDefault();
        if(datos.user !== "" && datos.password !== ""){
            localStorage.setItem('_authToken', btoa(JSON.stringify(datos)));
            history.push("/dashboard");        
        }else{
            M.toast({html: "USUARIO Y CONTRASEÑA REQUERIDO" , classes: 'red rounded'})
        }
    }

  return (
    <div className="container container-main">
        <div className="section">
            <div className="row">
                <div className="col s1 m3"></div>
                <div className="col s10 m6 white z-depth-5 mt-30 p-20">
                    <h5 className="center mt-0">Iniciar Sesión</h5>
                    <form className="row mb-0 login-form">
                        <div className="input-field col s12">
                            <input type="text" id="user" autoComplete="off" name="user" onChange={handleInputChange}/>
                            <label htmlFor="user">Usuario</label>
                        </div>
                        <div className="input-field col s12">
                            <input type="password" id="password" autoComplete="off" name="password" onChange={handleInputChange}/>
                            <label htmlFor="password">Password</label>
                        </div>
                        <div className="row center">
                            <button className="btn waves-effect waves-light pink accent-4" onClick={loginUser}>Entrar</button>
                        </div>
                        <div className="row left mb-0 "></div>
                    </form>

                    <div className="col s1 m3"></div>
                </div>
            </div>
        </div>
    </div>
  );
}
import React from 'react';
import './dataUser.scss';
import { useSelector } from 'react-redux';


export default function DataUser() {

    const user = useSelector((state) => state.userData)

  return (
    <div className="row data-user">
        <div className="col s8 offset-s2 data-content z-depth-3">
            <div className="input-field ">
                <h4 className="center-align">{user.user}</h4>
            </div>        
        </div>
    </div>
  );
}
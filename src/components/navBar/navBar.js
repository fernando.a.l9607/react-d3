import React, { Component } from 'react';
import './navBar.scss';
import M from "materialize-css";
import { Link } from 'react-router-dom';

class NavBar extends Component {
    constructor () {
      super();
      this.state = {
  
      };
    }

    componentDidMount(){
        document.addEventListener('DOMContentLoaded', function() {
            const elems = document.querySelectorAll('.sidenav');
            M.Sidenav.init(elems, "");
        });
    }

    clossSession(){
        localStorage.removeItem('_authToken');
    }

    render() {
        return (
            <nav className="cyan accent-4">
            <div className="nav-wrapper cyan accent-4">
            <Link className="brand-logo" id="LinkId"  to={{pathname: '/'}}>React D3.js</Link>
                <ul className="right hide-on-med-and-down">
                    <li>
                        <Link id="LinkId"  to={{pathname: '/'}} onClick={this.clossSession.bind(this)} >Cerrar sesión</Link>
                    </li>
                </ul>
    
                <ul id="nav-mobile" className="sidenav">
                    <li>
                        <Link id="LinkId"  to={{pathname: '/'}} onClick={this.clossSession.bind(this)} >Cerrar sesión</Link>
                    </li>
                </ul>
                <a href="..." data-target="nav-mobile" className="sidenav-trigger"><i className="material-icons">menu</i></a>
            </div>
            </nav>    
        );
    }
  
}

export default NavBar;
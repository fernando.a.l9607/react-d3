import React, {useEffect} from 'react'
import { Redirect } from "react-router-dom";
import NavBar from "../navBar/navBar";
import './dashboard.scss'
import { useSelector, useDispatch } from 'react-redux';
import json from "../../utils/data";
import DataUser from '../dataUser/dataUser';
import SunburstGraph from './sunburstGraph/sunburstGraph';



export default function Dashboard() {

    const dispatch = useDispatch()
    const dataDashboard = useSelector((state) => state)



    useEffect(() => {

        const token = localStorage.getItem("_authToken");
        if(token !== null){
            dispatch({
                type: 'SET_USER_DATA',
                payload: JSON.parse(atob(token))
            });
            dispatch({
                type: 'SET_INFO_GRAPH',
                payload: json
            });

        }else{
            return <Redirect to="/" />
        }

    }, [dispatch])


  return (

    <div className="item header">
        <NavBar/>
        <div className="header-background"></div>
        <header className="App-header">
            <DataUser/>
            <div className="row s12">
                <div className="col s10 offset-s1 z-depth-3">
                    <SunburstGraph dataDashboard={dataDashboard.dataInfoGrapht}/>
                </div>
            </div>
        </header>
    </div>

  );
}
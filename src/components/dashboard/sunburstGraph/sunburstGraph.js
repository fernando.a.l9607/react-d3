import React, { Component } from 'react';
import './sunburstGraph.scss';
import Sunburst from 'react-d3-zoomable-sunburst';



class SunburstGraph extends Component {
    constructor (props) {
      super();
      this.state = {
        dataDashboard:null,
          selectData:null,
      };
    }

    shouldComponentUpdate(nextProps, nextState){
        if(nextProps.dataDashboard !== nextState.dataDashboard){
            this.setState({dataDashboard: nextProps.dataDashboard})
        }
        return true
    }
    
    onSelect(event){
        this.setState({selectData: event.data})
    }


    render() {
        return (
            <div className="row">
                <div className="col s12 m12 xl5 content-right">
                    <Sunburst
                        onSelect={this.onSelect.bind(this)}
                        data={this.state.dataDashboard}
                        scale="linear"
                        tooltipContent={ <div className="sunburstTooltip" /> }
                        tooltip
                        tooltipPosition="right"
                        keyId="anagraph"
                        value="minSize"
                        width="400"
                        height="400"
                    />
                </div>
                <div className="col s12 m12 xl5">
                    <table>
                        <thead>
                            <tr>
                                <th>Género</th>
                                <th>Número de población</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>{this.state.selectData !== null ? this.state.selectData["name"] : ""}</td>
                                <td>{this.state.selectData !== null ? this.state.selectData["minSize"] : ""}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
  
}

export default SunburstGraph;
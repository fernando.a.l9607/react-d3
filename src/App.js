import React from 'react';
import M from "materialize-css";
import 'materialize-css/dist/css/materialize.min.css';
import './app.scss'
import Login from "./components/login/login";
import Dashboard from "./components/dashboard/dashboard";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './redux/reducer'

const initialState = {
  userData: [],
  dataInfoGrapht:[],
  selectData:[]
}

const store = createStore(reducer ,initialState)


function App() {
  M.AutoInit();
  return (
    <Provider store={store}>
      <Router>
        <div className="app">
          <div className="item content">
            <Switch>
              <Route exact path="/" component={Login} />
              <Route exact path="/dashboard" component={Dashboard} />
            </Switch>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;

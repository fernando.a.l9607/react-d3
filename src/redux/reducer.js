export default function reducer(state, action) {
    switch (action.type) {
        case 'SET_USER_DATA':{
          return {...state, userData: action.payload}
        }
        case 'SET_INFO_GRAPH':{
          return {...state, dataInfoGrapht: action.payload}
        }
        default: {
          return state
        } 
    }
}